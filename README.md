Vcheck is a Django app which provides an API for checking the latest and the minimum version of apps.

It could be used as a check for updates method at startup.

Install
=======
* Setup you django project or add it to an existing one.
* Add `check.apps.CheckConfig` to `INSTALLED_APPS` of your settings.
* Add `path('check', include('check.urls')),` to your urls.
* Run `makemigrations check` and `migrate` through `manage.py`.

Use
===
First we add our apps in admin panel ny naming them.

Then we can add some links for every app specified there.

After every release, we add that release with some descriptions and a flag to show if it's a forced update or not. Then we can change the latest flag for every link to that app.

API
===
You can send a get request like this:
```
https://website.url/check?os=Android
```
to get such a response:
```
{
  "latest": "0.1.3",
  "latest_desc": "fix some typos",
  "minimum": "0.1.0",
  "minimum_desc": "first semi-stable release",
  "links": {
    "F-Droid": "https://f-droid.org/packages/my.app",
    "Play Store": "https://play.google.com/store/apps/details?id=my.app",
    "Direct APK": "https://website.url/apps/myapp_latest.apk"
  }
}
```
