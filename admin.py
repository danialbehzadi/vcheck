from django.contrib import admin
from .models import App, Release, Link

# Register your models here

admin.site.register(App)
admin.site.register(Release)
admin.site.register(Link)
