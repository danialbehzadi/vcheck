from django.db import models

# Create your models here.


class App(models.Model):
    os = models.CharField(max_length=16)

    def __str__(self):
        return str(self.os)


class Release(models.Model):
    app = models.ForeignKey(App, on_delete=models.CASCADE)
    version = models.CharField(max_length=16)
    description = models.TextField()
    force = models.BooleanField(default=False)

    def __str__(self):
        return '{} ({})'.format(self.version, self.app)


class Link(models.Model):
    app = models.ForeignKey(App, on_delete=models.CASCADE)
    name = models.CharField(max_length=16)
    url = models.URLField(max_length=200)
    latest = models.BooleanField(default=True)

    def __str__(self):
        return str(self.name)
