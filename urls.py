#!/usr/bin/python3
# Released under GPLv3+ License
# Danial Behazdi <dani.behzi@ubuntu.com>, 2021

from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from . import views

urlpatterns = [
    path('', csrf_exempt(views.check))
    ]
