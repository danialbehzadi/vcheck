"""
main view for check app
"""
from json import dumps as jsonify

from django.http import StreamingHttpResponse
from django.shortcuts import get_object_or_404

from .models import App, Release, Link

# Create your views here.


def check(request):
    """
    main function for check app
    """
    app = request.GET.get('os')
    if not app:
        return StreamingHttpResponse('No OS specified', status=400)
    app = get_object_or_404(App, os=app)
    releases = Release.objects.filter(app=app).order_by('-version')
    try:
        latest = releases[0]
    except IndexError:
        return StreamingHttpResponse('No release', status=400)
    for release in releases:
        if release.force:
            minimum = release
            break
        minimum = latest
    links = Link.objects.filter(app=app, latest=True)
    links_item = {link.name: link.url for link in links}
    result = {
        'latest': latest.version,
        'latest_desc': latest.description,
        'minimum': minimum.version,
        'minimum_desc': minimum.description,
        'links': links_item,
        }

    print(result)
    return StreamingHttpResponse(jsonify(result), status=200)
